var request = require("request");
var redis = require("redis");
var fs = require("fs");
var querystring = require("querystring");

var client = redis.createClient();
client.on("connect", function() {
    console.log("Connected to Redis from Jasmine.");
});
    
describe("The API", function() {

    it("should respond to a GET request at /count", function(done) {    
        // insert test data into Redis
        client.set("count", 300, function() {
            console.log("The 'count' parameter set to 300.");
            // test the response to the get request
            request.get("http://localhost:8888/count", function (err, res, body) {
                expect(res.statusCode).toBe(200);
                client.get("count", function(err, reply) {
                    expect(body).toEqual("The count parameter retrieved from Redis: 300.");
                });
                done();
            });
        });
    });
    
    it("should not respond to a POST request at /count", function(done) {
        request.post("http://localhost:8888/count", function (err, res, body) {
                expect(res.statusCode).toBe(400);
                expect(body).toEqual("The server can process only GET requests on route /count.");
                done();
            });
    });
    
    it("should respond to a POST request at /track", function(done) {
        var qs = "count=12345&test_par1=ABC&test_par2=DEF";
        request.post(
            {
                headers: {"content-type" : "application/x-www-form-urlencoded" },
                url:     "http://localhost:8888/track",
                body:    qs
            }, function (err, res, body) {
                expect(res.statusCode).toBe(200);
                setTimeout(function() { // wait for the value to be updated in the DB
                    client.get("count", function(err, reply) {
                        expect(reply).toEqual((300 + 12345).toString());                
                    }); 
                }, 1000);
                
                fs.readFile("./src/queryStrings.json", "utf8", function(err, data) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    var post = JSON.stringify(querystring.parse(qs));
                    var jsonFile = JSON.parse(data);
                    // select the last entry -- overcomplicated, but I couldn't see other way
                    var lastEntry = jsonFile[Object.keys(jsonFile).sort().pop()];
                    expect(JSON.stringify(lastEntry)).toEqual(post);
                    done();
                });
        });
    });
    
    it("should not respond to a GET request at /track", function(done) {
        request.get(
            {
                "url": "http://localhost:8888/track"
            }, function(err, res, body) {
                expect(res.statusCode).toBe(400);
                expect(body).toEqual("The server can process only POST requests on route /track.");
                done();
            });
    });
});

setTimeout(function() {
    client.end(true);
}, 5000);