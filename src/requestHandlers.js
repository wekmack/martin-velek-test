var querystring = require("querystring");
var fs = require("fs");
var url = require("url");

function track(response, request, dbClient) {
    console.log("Request handler 'track' was called.");
    if (request.method == "POST") { // only POST request allowed on this route
        var body = "";
        request.on("data", function(data) {
            body += data;
        });
        request.on("end", function() {
            var post = querystring.parse(body);
            
            
            /*fs.writeFile("queryStrings.json", JSON.strin gify(post) + ",\n", {flag : "a"}, 
                     function (err) {
                        console.log("The posted data were saved to the file querystring.json.");
                        if (!isNaN(post["count"]) && post["count"] > 0) {
                            dbClient.incrby("count", post["count"], function(err, reply) {
                                console.log("Parameter count increased by " + post["count"] + ".");
                            });
                        }
                        serveMainPage(response);
            });*/

            function readStoredData(err, data) {
                if (err) {
                    console.log(err);
                }
                var jsonFile = JSON.parse(data);
                jsonFile[Date.now().toString()] = post;
                fs.writeFile("queryStrings.json", JSON.stringify(jsonFile, null, 2), {flag : "w"}, writeNewData);
            }
            
            function writeNewData(err) {
                if (err) {
                    console.log(err);
                }
                if (!isNaN(post["count"]) && post["count"] > 0) {
                    dbClient.incrby("count", post["count"], function(err, reply) {
                        console.log("Parameter count increased by " + post["count"] + ".");
                    });
                }
            }
            fs.readFile("queryStrings.json", "utf8", readStoredData);
            serveMainPage(response)
        });
    } else {
        responseError(response, "The server can process only POST requests on route /track.");
    }
}

function count(response, request, dbClient) {
    console.log("Request handler 'count' was called.");
    if (request.method == "GET") { // only GET requests allowed on this route
        dbClient.get("count", function(err, reply) {
                response.writeHead(200, {"Content-Type": "text/plain"});
                response.write("The count parameter retrieved from Redis: " + reply + ".");
                response.end();
        });
    } else {
        responseError(response, "The server can process only GET requests on route /count.");
    }
}

function home(response, request, dbClient) {
    console.log("Request handler 'home' was called.");
    serveMainPage(response);
}

function file(response, request, dbClient) {
    console.log("Request handler 'file' was called.");
    var exit = false;
    var path = "." + url.parse(request.url).pathname;
    if (path.endsWith(".html")) {
        response.writeHead(200, {"Content-Type": "text/html"});
    } else if (path.endsWith(".css")) {
        response.writeHead(200, {"Content-Type": "text/css"});
    } else if (path.endsWith(".js")) {
        response.writeHead(200, {"Content-Type": "text/javascript"});
    } else {
        console.log("No handler for the given document type.");
        exit = true;
    }
    if (exit) {
        response.end();
    } else {
        fs.readFile(path, function(err, file) {
            if (err) {
                throw err; 
            }
            response.write(file);
            response.end();
        });
    }
}

// >>> private helper methods
function responseError(response, errText) {
    response.writeHead(400, {"Content-Type": "text/plain"});
    response.write(errText);
    response.end();
}

function serveMainPage(response) {
    serveFile(response, "./main.html", "text/html");
}

function serveFile(response, filePath, mimeType) {
    fs.readFile(filePath, function(err, file) {
        if (err) {
            throw err; 
        }
        response.writeHead(200, {"Content-Type": mimeType});
        response.write(file);
        response.end();
    });
}
// <<<

exports.track = track;
exports.count = count;
exports.home = home;
exports.file = file;