var request = require("request");
var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var redis = require("redis");


var client = redis.createClient();
client.on("connect", function() {
    console.log("Connected to Redis.");
    client.set("count", 0, function() {
        console.log("Count parameter created in Redis.")
    });
});

var handle = {};
handle["/"] = requestHandlers.home;
handle["/main.html"] = requestHandlers.home;
handle["/custom.css"] = requestHandlers.file;
handle["/validator.js"] = requestHandlers.file;

handle["/track"] = requestHandlers.track;
handle["/count"] = requestHandlers.count;


server.start(router.route, handle, client);