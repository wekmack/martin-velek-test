function validateOnInput(event) {
    var count = event.target.value;
    var helpText = document.getElementById("help-text");
    var countContainer = document.getElementById("count-container");
    var confirm = document.getElementById("confirm");
    if (isNaN(count) || count < 0) {
        var errorMessage = "Input invalid! Pick a natural number please.";
        helpText.innerHTML = errorMessage;
        countContainer.classList.add("has-error");
        confirm.setAttribute("disabled", "true");
    } else {
        helpText.innerHTML = "";
        countContainer.classList.remove("has-error");
        confirm.removeAttribute("disabled");
    }
}